#Get Imagenet Class Service#

## How to use

There is only one endpoint `/predict`, that have `url` get parameter so it answers with classname, e.g:
```
GET http://localhost/predict?url=https://avatars.mds.yandex.net/get-pdb/70729/78b1f550-8755-4999-8a65-89e036faa2c2/s1200

{"image_class":"Chihuahua"}
```

The service is also deployed at http://134.209.224.169 so you can test it there


## Notes
* Flask was chosen as a framework as it is stateless and small service
* I did not have experience with ML so I just used very basic sample with pre-trained model
* PEP8 is used as code style with extended line length
* As there are no advanced endpoints for REST api I did not implement complete routing solution in terms of Flask (API blueprint etc)
* `/predict` endpoint shows only most valuable imagenet class name
* Imagenet classid -> name mapping is included as a python dict (`predictor.imagenet_class_index`). For 1 thousand small elements it is ok, but if this amount grows we will need to keep it in some key-value storage

## Limitations
* Not all corner cases are handled: e.g. if we send zip archive url instead of image url there will be 500
* Only two base integration tests was written so the logic is not fully covered with tests
