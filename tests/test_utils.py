from unittest.mock import patch

from classifier.predictor.imagenet_class_index import IMAGENET_INDEX_MAPPING
from classifier.predictor.utils import get_class_id, get_image_class_name


def test_get_correct_class_id(abs_image_path):
    class_id = get_class_id(abs_image_path)

    assert class_id in [970, 979]


def test_get_correct_image_class_name(abs_image_path):
    with patch('classifier.predictor.utils.current_request'), \
            patch('classifier.predictor.utils.requests'), \
            patch('classifier.predictor.utils.shutil'), \
            patch('classifier.predictor.utils.open'), \
            patch('classifier.predictor.utils.os') as mock_os:
        mock_os.path.join.return_value = abs_image_path

        class_name = get_image_class_name()

        assert class_name in [IMAGENET_INDEX_MAPPING[970], IMAGENET_INDEX_MAPPING[979]]
