import os

import pytest


@pytest.fixture(scope="module")
def abs_image_path():
    return os.path.abspath('tests/images/alps.jpg')
