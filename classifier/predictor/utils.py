import os
import shutil
import uuid

import requests
from requests.exceptions import ConnectionError, HTTPError

from PIL import Image
from flask import abort, request as current_request
from torch.autograd import Variable
from torchvision.models import vgg16
from torchvision import transforms

from classifier.predictor.imagenet_class_index import IMAGENET_INDEX_MAPPING
from classifier.settings import UPLOAD_FOLDER


def get_class_id(file_path):
    """
    Get path to the file and obtain imagenet class id using pretrained vgg model
    """
    net = vgg16(pretrained=True)

    centre_crop = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    img = Image.open(file_path)
    out = net(Variable(centre_crop(img).unsqueeze(0)))

    class_id = out[0].sort()[1][-1:].tolist()[0]

    return class_id


def get_image_class_name():
    """
    Download the image, obtain class and response with class name
    """
    image_url = current_request.args.get('url')

    if not image_url:
        abort(400, "Please specify a image url.")

    try:
        response = requests.get(image_url, stream=True, timeout=5)
        response.raise_for_status()
    except (ConnectionError, HTTPError):
        abort(400, "Not possible to obtain the image.")

    # compose file name for storing it temporarly in order to get classification
    file_name = image_url.rsplit('/', 1)[-1]

    # Mix name with uuid so name collision will not be possible
    file_name = uuid.uuid4().hex + file_name
    file_path = os.path.join(UPLOAD_FOLDER, file_name)

    with open(file_path, 'wb') as file:
        response.raw.decode_content = True
        shutil.copyfileobj(response.raw, file)

    class_id = get_class_id(file_path)

    return IMAGENET_INDEX_MAPPING[class_id]
