from flask import Flask, jsonify

from classifier.predictor.utils import get_image_class_name


app = Flask(__name__)


@app.route('/')
def info():
    return jsonify(
        {"info": "Imagenet classifier. Provide an image and get corresponding class name."}
    )


@app.route('/predict', methods=['GET'])
def predict():
    imagenet_class = get_image_class_name()

    return jsonify({"image_class": imagenet_class})


if __name__ == '__main__':
    app.run(debug=True)
