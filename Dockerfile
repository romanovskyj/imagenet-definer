FROM python:3.7
ADD ./classifier /classifier
ADD ./requirements.txt /classifier/requirements.txt
WORKDIR /classifier
RUN pip install -r requirements.txt
EXPOSE 80
ENTRYPOINT ["/usr/local/bin/gunicorn", "app:app"]
CMD ["-b", "0.0.0.0:80", "-w", "4"]
